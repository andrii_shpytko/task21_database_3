USE StoredPr_DB; 
-- drop table if exists street;
-- drop table if exists post;
-- drop table if exists zone;
-- drop table if exists medicine;
-- drop table if exists pharmacy;
-- drop table if exists employee;
-- drop table if exists pharmacy_medicine;
-- drop table if exists medicine_zone;

INSERT INTO street VALUES 	('Svobody Ave'),
							('Teatralna'),
                            ('V. Chornovola Ave'),
                            ('Kniazia Romana');
                            
INSERT INTO post VALUES ('лікар-продавець'),
						('продавець');
                        
INSERT INTO zone VALUES (1, 'heart'),
						(2, 'legs'),
                        (3, 'brain'),
                        (4, 'liver');
					
INSERT INTO medicine VALUES (1, 'tablet1', '123456789A', 1, 0, 0),
						(2, 'tablet2', '123456789B', 1, 1, 1),
                        (3, 'tablet3', '123456789C', 0, 0, 0),
                        (4, 'tablet4', '123456789D', 1, 0, 1),
                        (5, 'tablet5', '123456789E', 1, 1, 0);
                        
INSERT INTO pharmacy VALUES (DEFAULT, 'Podoroznyk', 4, 'apteka@gmail.com', '06:00:00', 1, 1, 'V. Chornovola Ave'),
							(DEFAULT, 'Health+', 24, 'Health@gmail.com', '08:00:00', 1, 1, 'Teatralna Street');
                            
INSERT INTO employee VALUES (DEFAULT, 'Petrov', 'Jhon', 'Ivanovych', '1234567891', 'VK-1010101', 21.1, '1966-12-23', 'лікар-продавець', 1),
							(DEFAULT, 'Ivanova', 'Anna', 'Petrivna', '1234567892', 'VK-1010102', 34.1, '1988-12-23', 'продавець', 1);
                            

INSERT INTO pharmacy_medicine VALUES (1,1), 
									(1,2),
                                    (1,3),
                                    (2,1),
                                    (2,4),
                                    (2,5);
                                    
INSERT INTO medicine_zone VALUES 	(1,1),
									(1,2),
                                    (1,3),
                                    (2,3),
                                    (3,1),
                                    (4,1),
                                    (4,3),
                                    (4,4),
                                    (5,4);
