Procedure 1
delimiter //
create procedure add_employee( 
	in surname varchar(30), 
    in name char(30), 
    in midle_name varchar(30), 
    in identity_number char(10), 
    in passport char(10), 
    in experience decimal (10, 1), 
    in birthday date, 
    in post varchar(15),
    in pharmacy_id int(11)
)
begin 
	insert into employee(surname, name, midle_name, identity_number, passport, experience, birthday, post, pharmacy_id) 
    values 
		(surname, name, midle_name, identity_number, passport, experience, birthday, post, pharmacy_id); 
end//
delimiter ;

drop procedure add_employee; 
#-------------------------------------------------------------------------------------------
drop procedure add_zone_for_medicine; 
Procedure 2
delimiter //
create procedure add_zone_for_medicine(
	in medicine_id int, 
    in zone_id int
)
begin
	if ( 
			(select 1 from medicine where id = medicine_id) != 0 
			and (select 1 from zone where id = zone_id) 
        ) 
    then 
		insert into medicine_zone (medicine_id, zone_id) values (medicine_id, zone_id); 
	end if;
end //
delimiter ;
#---------------------------------------------------------------------------------

drop procedure creating_table_with_employee_names; 
Procedure 3 
delimiter //
create procedure creating_table_with_employee_names() 
begin 
	declare done int default false; 
    declare firstName, lastName varchar(30); 
	declare quantity_column int; 
	declare employee_cursor cursor for select name, surname from employee; 
    declare continue handler for not found set done = true;

	open employee_cursor; 
		employee_loop: loop
			fetch employee_cursor into firstName, lastName;
			if done = true 
				then leave employee_loop; 
			end if; 
			
			set quantity_column = FLOOR(RAND() * (9 - 1) + 1); 
			if quantity_column = 1 then 
				set @temp_query := CONCAT('create table ', firstName, '_', lastName, '(c1 int)'); 
			elseif quantity_column = 2 then 
				set @temp_query := CONCAT('create table ', firstName, '_', lastName, '(c1 int, c2 int)'); 
			elseif quantity_column = 3 then 
				set @temp_query := CONCAT('create table ', firstName, '_', lastName, '(c1 int, c2 int, c3 int)'); 
			elseif quantity_column = 4 then 
				set @temp_query := CONCAT('create table ', firstName, '_', lastName, 
					'(c1 int, c2 int, c3 int, c4 int)'); 
			elseif quantity_column = 5 then 
				set @temp_query := CONCAT('create table ', firstName, '_', lastName, 
					'(c1 int, c2 int, c3 int, c4 int, c5 int)'); 
			elseif quantity_column = 6 then 
				set @temp_query := CONCAT('create table ', firstName, '_', lastName, 
					'(c1 int, c2 int, c3 int, c4 int, c5 int, c6 int)'); 
			elseif quantity_column = 7 then 
				set @temp_query := CONCAT('create table ', firstName, '_', lastName, 
					'(c1 int, c2 int, c3 int, c4 int, c5 int, c6 int, c7 int)'); 
			elseif quantity_column = 8 then 
				set @temp_query := CONCAT('create table ', firstName, '_', lastName, 
					'(c1 int, c2 int, c3 int, c4 int, c5 int, c6 int, c7 int, c8 int)'); 
			else 
				set @temp_query := CONCAT('create table ', firstName, '_', lastName, 
					'(c1 int, c2 int, c3 int, c4 int, c5 int, c6 int, c7 int, c8 int, c9 int)'); 
			end if;
			prepare employee_query from @temp_query; 
			execute employee_query; 
			deallocate prepare employee_query; 
		end loop; 
	close employee_cursor;
end //
delimiter ;